#! /usr/bin/make -f

CC=cl65
CFLAGS=-Os $(CDEBUG)
AS=ca65 -t c64
ASFLAGS=$(ASDEBUG)
LD=cl65 -t c64
LDFLAGS=-C memorymap.cfg
ifdef LIBDIR
LDFLAGS+=-L $(LIBDIR)
ASFLAGS+=-I $(LIBDIR)
CFLAGS+=-I $(LIBDIR)/include
endif
TARGET=scenelife

VPATH = src
LIBT7D ?= libT7D.lib

all:	$(TARGET).prg

$(TARGET).prg:	$(TARGET).raw
	pucrunch $^ $@

$(TARGET).raw:	     main.o
	$(LD) -o $@ -m $(basename $@).map -Ln $(basename $@).label $^ $(LIBT7D) LAMAlib.lib

.phony: clean run
clean:
	rm -f *.o *.raw *.map *.label
	rm -f $(TARGET) $(TARGET).prg
